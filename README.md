# Examples on how to use this API

##Get all products that are stored in french (use GET)

http://151.236.218.251/public/api/products?locale=fr-ch

##Create a product (use POST)

http://151.236.218.251/public/api/products
Make the payload have the following keys:

name
desc
price
locale (defaults to en-GB)

##Update a product (assuming 4 exists)

http://151.236.218.251/public/api/products/4 (use PUT)
Make the payload x-www-form-urlencoded using the same keys as above.

##Delete a product (assuming 4 exists)

http://151.236.218.251/public/api/products/4 (use DELETE)

