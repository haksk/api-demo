<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ApiController extends Controller
{
    protected $primaryKey = 'product_id';
    protected $locale = 'en-GB'; 

    public function getAllProducts(Request $request) 
    {
        if (!empty($request->locale)) {
            $this->setLocale($request->locale);
        }
        $allProducts = Product::where('product_locale', $this->locale)->get();
        return response()->json($allProducts);
    }

    public function createProduct(Request $request) 
    {
        $product = new Product;
        $product->product_name = $request->name;
        $product->product_desc = $request->desc;
        $product->product_category = $request->category;
        $product->product_price = $request->price;
        $product->product_locale = !empty($request->locale) ? $request->locale : $this->locale;
        $product->save();

        return response()->json([
            "message" => "Product record created"
        ], 201);
    }

    public function getProduct($id) 
    {
        $product = Product::find($id);

        return response()->json(['id: ' . $id, $product]);
    }
    /**
     * Please when sending data to this endpoint using PUT,
     * set the body to be x-www-form-urlencoded or the request
     * body will be empty.
    */

    public function updateProduct(Request $request, $id) 
    {
        $product = Product::find($id);

        if (!empty($request->name)) {
            $product->product_name = $request->name;
        }
        if (!empty($request->desc)) {
            $product->product_desc = $request->desc;
        }
        if (!empty($request->price)) {
            $product->product_price = $request->price;
        }
        if (!empty($request->locale)) {
            $product->product_locale = $request->locale;
        }
        $product->save();

        return response()->json([
            "message" => "Product record updated"
        ], 201);

    }

    public function deleteProduct ($id) 
    {
        $product = Product::find($id);
	$product->delete();

        return response()->json([
            "message" => "Product record deleted"
        ], 201);

    }
    /**
     * @param string $locale
     * @return void
     *
     * Switch locale as and when needed. 
    */
    protected function setLocale($locale)
    {
        $this->locale = $locale;
    }
}
