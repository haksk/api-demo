<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'product_id';
    protected $table = 'products';
    protected $fillable = ['product_name', 'product_desc', 'product_category', 'product_price'];
    public $timestamps = false;
}
