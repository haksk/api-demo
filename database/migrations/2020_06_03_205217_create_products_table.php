<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        CREATE TABLE `products` (
          `product_id` int(11) NOT NULL AUTO_INCREMENT,
          `product_name` varchar(255) DEFAULT NULL,
          `product_desc` text DEFAULT NULL,
          `product_category` varchar(20) DEFAULT NULL,
          `product_price` float DEFAULT NULL,
          `product_locale` char(5) DEFAULT "en-GB",
          PRIMARY KEY (`product_id`)
        )');
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
